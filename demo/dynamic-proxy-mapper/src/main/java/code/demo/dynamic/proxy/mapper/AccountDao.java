package code.demo.dynamic.proxy.mapper;

import code.demo.dynamic.proxy.mapper.annotation.Mapper;

/**
 * AccountDao.java
 *
 * @author BlueDriver
 * @email cpwu@foxmail.com
 * @date 2022/7/10 17:13
 * -----------------------------------------
 * Gitee: https://gitee.com/BlueDriver
 * Github: https://github.com/BlueDriver
 * -----------------------------------------
 */
@Mapper
public interface AccountDao {
    String getAccountNameById(int id);
}
