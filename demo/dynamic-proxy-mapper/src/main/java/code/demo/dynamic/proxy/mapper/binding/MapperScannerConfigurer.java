package code.demo.dynamic.proxy.mapper.binding;

import code.demo.dynamic.proxy.mapper.annotation.Mapper;
import code.demo.dynamic.proxy.mapper.utils.ClassFindUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionReaderUtils;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.ClassUtils;
import org.springframework.util.StringUtils;

import java.beans.Introspector;
import java.util.Set;

/**
 * MapperScannerConfigurer.java
 *
 * @author BlueDriver
 * @email cpwu@foxmail.com
 * @date 2022/7/10 16:41
 * -----------------------------------------
 * Gitee: https://gitee.com/BlueDriver
 * Github: https://github.com/BlueDriver
 * -----------------------------------------
 */
@Component
public class MapperScannerConfigurer implements BeanDefinitionRegistryPostProcessor, EnvironmentAware, ApplicationContextAware {
    /**
     * 日志对象
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(MapperScannerConfigurer.class);

    /**
     * mapper类包路径
     */
    private String mapperPackage;
    /**
     * applicationContext
     */
    private ApplicationContext applicationContext;


    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        // 包路径
        String[] basePackages = StringUtils.tokenizeToStringArray(mapperPackage,
                ConfigurableApplicationContext.CONFIG_LOCATION_DELIMITERS);

        // 在包路径下查找含指定注解的类
        Set<Class<?>> classSet = ClassFindUtils.findClass(Mapper.class, basePackages);

        for (Class<?> clazz : classSet) {
            // 将找到的含Mapper注解的类进行bean定义和注册
            GenericBeanDefinition beanDefinition = new GenericBeanDefinition();

            beanDefinition.setBeanClass(MapperFactoryBean.class);
            beanDefinition.setScope(BeanDefinition.SCOPE_SINGLETON);

            // 设置构造参数（对应 MapperFactoryBean 的构造器）
            beanDefinition.getConstructorArgumentValues().addGenericArgumentValue(clazz);
            beanDefinition.getConstructorArgumentValues().addGenericArgumentValue(applicationContext);

            // bean名称采取类名首字母小写的形式
            String beanName = Introspector.decapitalize(ClassUtils.getShortName(clazz));

            BeanDefinitionHolder holder = new BeanDefinitionHolder(beanDefinition, beanName);
            BeanDefinitionReaderUtils.registerBeanDefinition(holder, registry);
            LOGGER.info("为[{}]注册名称为[{}]的bean", clazz.getName(), beanName);
        }
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        // do nothing
    }

    /**
     * 从环境配置中读取mapper包路径
     */
    @Override
    public void setEnvironment(Environment environment) {
        mapperPackage = environment.getProperty("code.demo.mapper.package", "code.demo");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}