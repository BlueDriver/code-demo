package code.demo.dynamic.proxy.mapper.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Mapper.java
 *
 * @author BlueDriver
 * @email cpwu@foxmail.com
 * @date 2022/7/10 16:31
 * -----------------------------------------
 * Gitee: https://gitee.com/BlueDriver
 * Github: https://github.com/BlueDriver
 * -----------------------------------------
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Mapper {
}