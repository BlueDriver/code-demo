package mine.code.test.log4j.dto;

import java.util.List;

/**
 * @author BlueDriver
 * @email cpwu@foxmail.com
 * @date 2022/7/4 19:09
 * -----------------------------------------
 * Gitee: https://gitee.com/BlueDriver
 * Github: https://github.com/BlueDriver
 * -----------------------------------------
 */
public class Job {
    /**
     * jobName
     */
    private String jobName;

    /**
     * salary
     */
    private int salary;

    /**
     * company
     */
    private String company;

    /**
     * address
     */
    private String address;

    /**
     * tel
     */
    private String tel;

    /**
     * position
     */
    private List<String> position;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }


    @Override
    public String toString() {
        return "Job{" +
                "jobName='" + jobName + '\'' +
                ", salary=" + salary +
                ", company='" + company + '\'' +
                ", address='" + address + '\'' +
                ", tel='" + tel + '\'' +
                ", position=" + position +
                '}';
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public List<String> getPosition() {
        return position;
    }

    public void setPosition(List<String> position) {
        this.position = position;
    }
}
