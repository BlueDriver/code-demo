package mine.code.test.logback.dto;

import java.util.Date;

/**
 * @author BlueDriver
 * @email cpwu@foxmail.com
 * @date 2022/7/4 19:09
 * -----------------------------------------
 * Gitee: https://gitee.com/BlueDriver
 * Github: https://github.com/BlueDriver
 * -----------------------------------------
 */
public class User {
    /**
     * name
     */
    private String name;

    /**
     * idCard
     */
    private String idCard;

    /**
     * cardNo
     */
    private String cardNo;

    /**
     * mobile
     */
    private String mobile;

    /**
     * tel
     */
    private String tel;

    /**
     * password
     */
    private String password;

    /**
     * email
     */
    private String email;

    /**
     * address
     */
    private String address;

    /**
     * birth
     */
    private Date birth;
    /**
     * job
     */
    private Job job;

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", idCard='" + idCard + '\'' +
                ", cardNo='" + cardNo + '\'' +
                ", mobile='" + mobile + '\'' +
                ", tel='" + tel + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", birth=" + birth +
                ", job=" + job +
                '}';
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }
}
