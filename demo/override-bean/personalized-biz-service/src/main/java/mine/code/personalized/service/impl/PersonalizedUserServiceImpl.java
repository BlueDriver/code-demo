package mine.code.personalized.service.impl;

import mine.code.config.annotation.OverrideBean;
import mine.code.standard.service.impl.StandardUserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


/**
 * @author BlueDriver
 * @email cpwu@foxmail.com
 * @date 2022/6/27 20:31
 * -----------------------------------------
 * Gitee: https://gitee.com/BlueDriver
 * Github: https://github.com/BlueDriver
 * -----------------------------------------
 */
@Component
@OverrideBean
public class PersonalizedUserServiceImpl extends StandardUserServiceImpl {
    /**
     * 日志对象
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonalizedUserServiceImpl.class);

    @Override
    public String getUserNameById(int id) {
        LOGGER.info("getUserNameById个性化实现");
        return "PersonalizedName_" + id;
    }

    @Override
    public String hello() {
        return "PersonalizedUserServiceImpl";
    }
}