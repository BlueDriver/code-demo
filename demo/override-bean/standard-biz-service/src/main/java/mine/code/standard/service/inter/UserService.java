package mine.code.standard.service.inter;

/**
 * @author BlueDriver
 * @email cpwu@foxmail.com
 * @date 2022/6/27 20:26
 * -----------------------------------------
 * Gitee: https://gitee.com/BlueDriver
 * Github: https://github.com/BlueDriver
 * -----------------------------------------
 */
public interface UserService {
    String hello();

    String getUserNameById(int id);
}
